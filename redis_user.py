import redis

r = redis.StrictRedis(host='localhost', port=6379)


def set_user(chat_id, step):
    r.set(chat_id, step)


def get_user(chat_id):
    try:
        return int(r.get(chat_id).decode('utf-8'))
    except KeyError:
        return 0


def get_trade(key):
    try:
        return r.get(key).decode('utf-8')
    except KeyError:
        return 0

def set_flag(flag):
    r.set("flag", flag)


def get_flag(key):
    try:
        return int(r.get(key).decode('utf-8'))
    except KeyError:
        return 0