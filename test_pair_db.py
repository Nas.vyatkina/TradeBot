import requests
import psycopg2
import json

str_connect_to_db = "dbname='rialto' user='postgres' host='127.0.0.1' password='test' port='5432'"


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except Exception:
        print("I am unable to connect to the database")
    return conn


def delete_pair_db(id_pair, id_user):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            cur.execute('''Delete 
                        from sessions
                        where (id_user = {} and sel_pair={})'''.format(id_user, id_pair))
            conn.commit()
            conn.close()
            return ''
        except Exception as e:
            print(e)


def test_pair_db():
    id_rialto = 1
    conn = create_connection()
    if conn:
        try:
            url = 'https://wex.nz/api/3/ticker/'
            cur = conn.cursor()
            cur.execute('''Select name_pair
                        from pair
                        where rialto=('{}')'''.format(id_rialto))
            rows = cur.fetchall()
            for row in rows:
                url += (str(row[0]) + '-')
            url = url[:-1] + '?ignore_invalid=1'
            r = requests.get(url).json()
            for row in rows:
                buy = r[row[0]]['buy']
                sale = r[row[0]]['sell']
                cur.execute('''Update pair set buy='{}', sale='{}'
                            where (name_pair='{}')'''.format(buy, sale, row[0]))
                conn.commit()
            cur.execute('''Select u.user_id, p.name_pair, p.buy, p.sale, s.buy_sale, u.id, p.id
                        from users as u 
                        join sessions as s on u.id = s.id_user
                        join pair as p on s.sel_pair = p.id''')
            rows = cur.fetchall()
            for row in rows:
                if float(row[4]['buy']['top']) != 0 or float(row[4]['buy']['buttom']) != 0:
                    if row[2] <= float(row[4]['buy']['buttom']) or row[2] >= float(row[4]['buy']['top']):
                        message = 'цена на покупку пары ' + str(row[1]) + ' равна ' + str(row[2])
                        json_rez = row[4]
                        if float(json_rez['sale']['buttom']) == 0 and float(json_rez['sale']['top']) == 0:
                            delete_pair_db(row[6], row[5])
                        else:
                            try:
                                json_rez['buy']['buttom'] = 0
                                json_rez['buy']['top'] = 0
                                cur.execute('''Update sessions set buy_sale= '{}'
                                            where (id_user = '{}' and sel_pair = '{}')'''.format(json.dumps(json_rez),
                                                                                                 row[5], row[6]))
                                conn.commit()
                            except Exception as e:
                                print(e)
                            conn.close()
                        params = {'chat_id': row[0], 'text': message}
                        url = 'https://api.telegram.org/bot445173704:AAHuCBusKZyTIbhdZtARL8ZqZv-aUK_N9Pg/sendMessage'
                        requests.post(url, data=params)
                if float(row[4]['sale']['top']) != 0 or float(row[4]['sale']['buttom']) != 0:
                    if row[3] <= float(row[4]['sale']['buttom']) or row[3] >= float(row[4]['sale']['top']):
                        message = 'цена на продажу пары ' + str(row[1]) + ' равна ' + str(row[3])
                        json_rez = row[4]
                        if float(json_rez['buy']['buttom']) == 0 and float(json_rez['buy']['top']) == 0:
                            delete_pair_db(row[6], row[5])
                        else:
                            json_rez['sale']['buttom'] = 0
                            json_rez['sale']['top'] = 0
                            cur.execute('''Update sessions set buy_sale= '{}'
                                        where (id_user = '{}' and sel_pair = '{}')'''.format(json.dumps(json_rez),
                                                                                             row[5], row[6]))
                            conn.commit()
                            conn.close()
                        params = {'chat_id': row[0], 'text': message}
                        url = 'https://api.telegram.org/bot445173704:AAHuCBusKZyTIbhdZtARL8ZqZv-aUK_N9Pg/sendMessage'
                        requests.post(url, data=params)
            conn.close()
            return ''
        except Exception as e:
            print(e)
            conn.close()


test_pair_db()