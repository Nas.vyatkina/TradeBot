from TelBot.config import create_connection
import psycopg2
import json
import uuid
from flask import session
#import traceback
import requests


def registration_db(user_id):
    conn = create_connection()
    #if login[0] != '' and email[0] != '' and phone[0] != '' and pas[0] != '':
    if conn:
        try:
            #key = uuid.uuid4()
            cur = conn.cursor()
            cur.execute('''Select * from users where (user_id = '{}')'''.format(user_id))
            rez = cur.fetchone()
            if not rez:
                cur.execute('''Insert INTO users (user_id)
                          VALUES ('{}')'''.format(user_id))
                #cur.execute('''Insert INTO users (nickname, email, phone, password, key_s)
                #          VALUES ('{}','{}','{}','{}', '{}')'''.format(login[0], email[0], phone[0], pas[0], key))

                conn.commit()
                conn.close()
                return 'ok'

        except psycopg2.IntegrityError:
            conn.close()
            return ''
    else:
        return ''


def price_pair(name_pair):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''Select buy, sale 
                    from pair
                    where (name_pair = '{}')'''.format(name_pair))
        price = cur.fetchone()
        conn.close()
        return price


def name_rialto():
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''Select name_rialto, id 
        from rialto''')
        rialto_all=cur.fetchall()
        conn.close()
        return rialto_all
    return 'err'


def id_rialto(name_rialto):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''Select id 
        from rialto
        where name_rialto=('{}')'''.format(name_rialto))
        rialto=cur.fetchone()
        conn.close()
        return rialto
    return 'err'


def id_pair(name_pair):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''Select id 
        from pair
        where name_pair=('{}')'''.format(name_pair))
        pair=cur.fetchone()
        conn.close()
        return pair
    return 'err'


def pair_info(name_pair):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''select pair.name_pair, pair.id, s.id_user, s.buy_sale, pair.buy, pair.sale
                  from pair
                  left join sessions as s 
                  on pair.id = s.sel_pair
                  where (pair.name_pair='{}') order by s.buy_sale->'buy'->>'buttom';'''.format(name_pair))

        trade = cur.fetchall()
        conn.close()
        return trade



def pair_user(id_trade, id_user):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''select pair.name_pair, pair.id, s.id_user, s.buy_sale, pair.buy, pair.sale
                  from pair
                  inner join (Select * from sessions where id_user = {}) as s 
                  on pair.id = s.sel_pair
                  where (pair.rialto={}) order by s.buy_sale->'buy'->>'buttom';'''.format(id_user, id_trade))

        trade = cur.fetchall()
        conn.close()
        return trade


def pair_trade(id_trade, id_user):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''select pair.name_pair, pair.id, s.id_user, s.buy_sale, pair.buy, pair.sale
                  from pair
                  left join (Select * from sessions where id_user != {}) as s 
                  on pair.id = s.sel_pair
                  where (pair.rialto={}) order by s.buy_sale->'buy'->>'buttom';'''.format(id_user, id_trade))

        trade = cur.fetchall()
        conn.close()
        return trade


def err_pair(trade, name_pair):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''select pair.name_pair, rialto.id, rialto.name_rialto
                          from pair
                          inner join rialto on pair.rialto = rialto.id
                          where (rialto.name_rialto='{}' and pair.name_pair = '{}') ;'''.format(trade, name_pair))

        trade = cur.fetchone()
        conn.close()
        return trade


def user_id(user_id):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute('''Select id 
                    from users
                    where (user_id ='{}')'''.format(user_id))
        id_db = cur.fetchone()
        conn.close
        return id_db


def save_pair_db(data, id_user):
    conn = create_connection()

    if conn:
        try:
            #if data['valueBuyMax'] != 0 or data['valueBuyMin']!= 0 or data['valueSaleMax'] != 0 or data['valueSaleMin'] !=0 :

            json_res={}
            json_res['buy']={'top':data['valueBuyMax'], 'buttom':data['valueBuyMin']}
            json_res['sale']= {'top':data['valueSaleMax'], 'buttom':data['valueSaleMin']}
            idTrade = int(data['idTrade'])
            idPair=int(data['idPair'])


            cur = conn.cursor()
            cur.execute('''Insert INTO sessions (id_user, id_rialto, sel_pair, buy_sale)
                            VALUES ('{}','{}','{}','{}')'''.format(id_user, idTrade, idPair, json.dumps(json_res)))
            conn.commit()
            #print(3)
            conn.close()
            return ''

        except psycopg2.IntegrityError:
            conn = create_connection()
            cur = conn.cursor()
            cur.execute('''Select buy_sale 
                        from sessions
                        WHERE (id_user = '{}' and id_rialto = '{}' and sel_pair = '{}')'''.format(id_user, idTrade, idPair))
            json_rez = cur.fetchone()
            json_rez = json_rez[0]
            if data['valueBuyMin']!=0:
                json_rez['buy']['buttom'] = data['valueBuyMin']

                cur.execute('''Update sessions set buy_sale= '{}'
                          where (id_user = '{}' and id_rialto = '{}' and sel_pair = '{}')'''.format(json.dumps(json_rez), id_user, idTrade, idPair))
                conn.commit()
            elif data['valueBuyMax'] != 0:
                json_rez['buy']['top'] = data['valueBuyMax']
                cur.execute('''Update sessions set buy_sale= '{}'
                          where (id_user = '{}' and id_rialto = '{}' and sel_pair = '{}')'''.format(json.dumps(json_rez), id_user, idTrade, idPair))
                conn.commit()
            elif data['valueSaleMin']!=0:
                json_rez['sale']['buttom'] = data['valueSaleMin']
                cur.execute('''Update sessions set buy_sale= '{}'
                          where (id_user = '{}' and id_rialto = '{}' and sel_pair = '{}')'''.format(json.dumps(json_rez), id_user, idTrade, idPair))
                conn.commit()
            elif data['valueSaleMax']!=0:
                json_rez['sale']['top'] = data['valueSaleMax']
                cur.execute('''Update sessions set buy_sale= '{}'
                          where (id_user = '{}' and id_rialto = '{}' and sel_pair = '{}')'''.format(json.dumps(json_rez), id_user, idTrade, idPair))
                conn.commit()
            #print(4)
            conn.close()
            return ''

        #except Exception as e:
        #    print(e)

def delete_pair_db(id_pair, id_user):
    conn = create_connection()

    if conn:
        try:
            cur = conn.cursor()
            cur.execute('''Delete 
            from sessions
            where (id_user = {} and sel_pair={})'''.format(id_user, id_pair))
            conn.commit()
            conn.close()
            return ''
        except Exception as e:
            print(e)




