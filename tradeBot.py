import telebot
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from work_db import registration_db, name_rialto, user_id, pair_trade, save_pair_db, \
    id_rialto, id_pair, err_pair, price_pair, pair_user, pair_info, delete_pair_db
from redis_user import set_user, get_user, get_trade, set_flag, get_flag
from config import token

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def handler_text(message):
    mes = "Выберите интересующую вас биржу"
    registration_db(message.from_user.id)
    set_user(message.chat.id, 0)
    user_markup = telebot.types.ReplyKeyboardMarkup(True)
    rows = name_rialto()
    for row in rows:
        user_markup.row(row[0])
    bot.send_message(message.from_user.id, mes, reply_markup=user_markup)


@bot.message_handler(content_types=['text'])
def handler_text(message):
    if message.text == "Назад":
        step = get_user(message.from_user.id)
        user_markup = telebot.types.ReplyKeyboardMarkup(True)
        if step == 1:
            mes = "Выберите интересующую вас биржу"
            set_user(message.chat.id, step - 1)
            rows = name_rialto()
            for row in rows:
                user_markup.row(row[0])
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 2:

            if message.text != "Выберите одно из предложенных действий":
                mes = "Выберите действие"
            else:
                mes = ''
            set_user(message.chat.id, 1)
            user_markup.row("ваши подписки")
            user_markup.row("другие пары")
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 3:
            flag = get_flag("flag")
            if flag == 1:
                rows = name_rialto()
                for row in rows:
                    id_db = user_id(message.from_user.id)
                    if get_trade("trade") == row[0]:
                        set_flag(1)
                        set_user(message.chat.id, 2)
                        trade = pair_user(row[1], id_db[0])
                        user_markup.row("Информация по подпискам")
                        user_markup.row("Назад")
                        if trade:
                            mes = "Выберите пару"
                            for pair in trade:
                                user_markup.row(pair[0])
                        else:
                            mes = "У вас пока нет подписок, вернитесь к выбору действия"
                        bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
            elif flag == 2:
                rows = name_rialto()
                for row in rows:
                    id_db = user_id(message.from_user.id)
                    if get_trade("trade") == row[0]:
                        set_flag(2)
                        set_user(message.chat.id, 2)
                        trade = pair_trade(row[1], id_db[0])
                        user_markup.row("Назад")
                        if trade:
                            mes = "Выберите пару"
                            for pair in trade:
                                user_markup.row(pair[0])
                        else:
                            mes = "Нет пар, вернуться к выбору действия"
                        bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 4 or step == 5 or step == 6:
            trade = get_trade("trade")
            pair = get_trade("pair")
            if err_pair(trade, pair) is not None and err_pair(trade, pair) != '':
                flag = get_flag("flag")
                mes = ''
                if flag == 1:
                    pair = pair_info(pair)[0]
                    mes = ("тек.курс: покупка:*" + str(pair[4]) + "*\t" + "продажа:*" + str(pair[5]) + "*\n" +
                           "пожелания покупки: ⬇" + str(pair[3]['buy']['buttom']) + "\t⬆" + str(pair[3]['buy']['top']) +
                           "\nпожелания продажи: ⬇" + str(pair[3]['sale']['buttom']) + "\t⬆" +
                           str(pair[3]['sale']['top']) + "\n")
                elif flag == 2:
                    pair = pair_info(pair)[0]
                    mes = ("тек.курс: покупка:*" + str(pair[4]) + "*\t" + "продажа:*" + str(pair[5]) + "*\n")
                mes += "Выберите действие"
                set_user(message.chat.id, 3)
                user_markup.row("покупка")
                user_markup.row("продажа")
                if flag == 1:
                    user_markup.row("отписаться")
            else:
                mes = "Выберите пару из списка"
                set_user(message.chat.id, 3)
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, text=mes, parse_mode='Markdown', reply_markup=user_markup)
    elif message.text == "Информация по подпискам":
        user_markup = telebot.types.ReplyKeyboardMarkup(True)
        rows = name_rialto()
        mes = ''
        for row in rows:
            id_db = user_id(message.from_user.id)
            if get_trade("trade") == row[0]:
                set_flag(1)
                set_user(message.chat.id, 3)
                trade = pair_user(row[1], id_db[0])
                for pair in trade:
                    mes += ('пара: *' + str(pair[0]) + "*\nтек.курс: покупка:*" + str(pair[4]) + "*\t" + "продажа:*" +
                            str(pair[5]) + "*\n" + "пожелания покупки: ⬇" + str(pair[3]['buy']['buttom']) + "\t⬆" +
                            str(pair[3]['buy']['top']) + "\nпожелания продажи: ⬇" + str(pair[3]['sale']['buttom']) +
                            "\t⬆" + str(pair[3]['sale']['top']) + "\n")
        user_markup.row("Назад")
        bot.send_message(message.from_user.id, text=mes, parse_mode='Markdown', reply_markup=user_markup)
    elif message.text == "отписаться":
        id_p = id_pair(get_trade("pair"))
        id_db = user_id(message.from_user.id)
        delete_pair_db(id_p[0], id_db[0])
        rows = name_rialto()
        user_markup = telebot.types.ReplyKeyboardMarkup(True)
        for row in rows:
            if get_trade("trade") == row[0]:
                set_flag(1)
                set_user(message.chat.id, 2)
                trade = pair_user(row[1], id_db[0])
                user_markup.row("Информация по подпискам")
                user_markup.row("Назад")
                if trade:
                    mes = "Выберите пару"
                    for pair in trade:
                        user_markup.row(pair[0])
                else:
                    mes = "У вас пока нет подписок, вернитесь к выбору действия"
                bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
    else:
        step = get_user(message.from_user.id)
        user_markup = telebot.types.ReplyKeyboardMarkup(True)
        if step == 0:
            if message.text != "Выберите одно из предложенных действий":
                mes = "Выберите действие"
            else:
                mes = ''
            set_user(message.chat.id, 1)
            set_user("trade", message.text)
            user_markup.row("ваши подписки")
            user_markup.row("другие пары")
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 1:
            if message.text == "ваши подписки":
                rows = name_rialto()
                for row in rows:
                    id_db = user_id(message.from_user.id)
                    if get_trade("trade") == row[0]:
                        set_flag(1)
                        set_user(message.chat.id, 2)
                        trade = pair_user(row[1], id_db[0])
                        user_markup.row("Информация по подпискам")
                        user_markup.row("Назад")
                        if trade:
                            mes = "Выберите пару"
                            for pair in trade:
                                user_markup.row(pair[0])
                        else:
                            mes = "У вас пока нет подписок, вернитесь к выбору действия"
                        bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
            elif message.text == "другие пары":
                rows = name_rialto()
                for row in rows:
                    id_db = user_id(message.from_user.id)
                    if get_trade("trade") == row[0]:
                        set_flag(2)
                        set_user(message.chat.id, 2)
                        trade = pair_trade(row[1], id_db[0])
                        user_markup.row("Назад")
                        if trade:
                            mes = "Выберите пару"
                            for pair in trade:
                                user_markup.row(pair[0])
                        else:
                            mes = "Нет пар, вернуться к выбору действия"
                        bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
            else:
                mes = "Выберите одно из предложенных действий"
                set_user(message.chat.id, 0)
                bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 2:
            trade = get_trade("trade")
            if err_pair(trade, message.text) is not None and err_pair(trade, message.text) != '':
                set_user("pair", message.text)
                flag = get_flag("flag")
                mes = ''
                if flag == 1:
                    pair = pair_info(message.text)[0]
                    mes = ("тек.курс: покупка:*" + str(pair[4]) + "*\t" + "продажа:*" + str(pair[5]) + "*\n" +
                           "пожелания покупки: ⬇" + str(pair[3]['buy']['buttom']) + "\t⬆" + str(pair[3]['buy']['top']) +
                           "\nпожелания продажи: ⬇" + str(pair[3]['sale']['buttom']) + "\t⬆" +
                           str(pair[3]['sale']['top']) + "\n")
                elif flag == 2:
                    pair = pair_info(message.text)[0]
                    mes = ("тек.курс: покупка:*" + str(pair[4]) + "*\t" + "продажа:*" + str(pair[5]) + "*\n")
                mes += "Выберите действие"
                set_user(message.chat.id, 3)
                user_markup.row("покупка")
                user_markup.row("продажа")
                if flag == 1:
                    user_markup.row("отписаться")
            else:
                mes = "Выберите пару из списка"
                set_user(message.chat.id, 3)
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, text=mes, parse_mode='Markdown', reply_markup=user_markup)
        elif step == 3:
            if message.text == "покупка":
                set_user(message.chat.id, 4)
                mes = "Введите нижний порог"
                user_markup.row("Назад")
                bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
            elif message.text == "продажа":
                set_user(message.chat.id, 6)
                mes = "Введите нижний порог"
                user_markup.row("Назад")
                bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 4:
            try:
                val_buy_min = float(message.text)
                id_user = user_id(message.from_user.id)
                trade = get_trade("trade")

                id_trade = id_rialto(trade)
                pair = get_trade("pair")
                pair_id = id_pair(pair)
                data = {"idTrade": id_trade[0],
                        "idPair": pair_id[0],
                        "valueBuyMin": val_buy_min,
                        "valueBuyMax": 0,
                        "valueSaleMin": 0,
                        "valueSaleMax": 0
                        }
                save_pair_db(data, id_user[0])
                set_user(message.chat.id, 5)
                mes = "Введите верхний порог"
            except ValueError:
                set_user(message.chat.id, 4)
                mes = "Неправильный ввод, введите число"
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 5:
            try:
                val_buy_max = float(message.text)
                id_user = user_id(message.from_user.id)
                trade = get_trade("trade")
                id_trade = id_rialto(trade)
                pair = get_trade("pair")
                pair_id = id_pair(pair)
                data = {"idTrade": id_trade[0],
                        "idPair": pair_id[0],
                        "valueBuyMin": 0,
                        "valueBuyMax": val_buy_max,
                        "valueSaleMin": 0,
                        "valueSaleMax": 0
                        }
                save_pair_db(data, id_user[0])
                mes = "Данные сохранены"
                set_user(message.chat.id, 3)
            except ValueError:
                set_user(message.chat.id, 5)
                mes = "Неправильный ввод, введите число"
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 6:
            try:
                val_sale_min = float(message.text)
                id_user = user_id(message.from_user.id)
                trade = get_trade("trade")
                id_trade = id_rialto(trade)
                pair = get_trade("pair")
                pair_id = id_pair(pair)
                data = {"idTrade": id_trade[0],
                        "idPair": pair_id[0],
                        "valueBuyMin": 0,
                        "valueBuyMax": 0,
                        "valueSaleMin": val_sale_min,
                        "valueSaleMax": 0
                        }
                save_pair_db(data, id_user[0])
                set_user(message.chat.id, 7)
                mes = "Введите верхний порог"
            except ValueError:
                set_user(message.chat.id, 6)
                mes = "Неправильный ввод, введите число"
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)
        elif step == 7:
            try:
                val_sale_max = float(message.text)
                id_user = user_id(message.from_user.id)
                trade = get_trade("trade")
                id_trade = id_rialto(trade)
                pair = get_trade("pair")
                pair_id = id_pair(pair)
                data = {"idTrade": id_trade[0],
                        "idPair": pair_id[0],
                        "valueBuyMin": 0,
                        "valueBuyMax": 0,
                        "valueSaleMin": 0,
                        "valueSaleMax": val_sale_max
                        }
                save_pair_db(data, id_user[0])
                mes = "Данные сохранены"
                set_user(message.chat.id, 3)
            except ValueError:
                set_user(message.chat.id, 7)
                mes = "Неправильный ввод, введите число"
            user_markup.row("Назад")
            bot.send_message(message.from_user.id, mes, reply_markup=user_markup)


bot.polling(none_stop=True)
